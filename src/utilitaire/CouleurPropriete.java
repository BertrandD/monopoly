package utilitaire;

import java.awt.Color;

public enum CouleurPropriete {
	bleuFonce("Bleu Fonce"), 
	orange("Orange"),
	mauve("Mauve"),
	violet("Violet"), 
	bleuCiel("Bleu Ciel"),
	jaune("Jaune"),
	vert("Vert"),
	rouge("Rouge");
	
	private String name;
	
	private CouleurPropriete(String couleur){
		this.name = couleur;
	}
	
	public String getLabel(){
		return name;
	}
	
	public Color getColor(){
		switch(this){
		case bleuFonce : return Color.BLUE;
		case bleuCiel : return Color.CYAN;
		case mauve : return Color.MAGENTA;
		case orange : return Color.ORANGE;
		case violet: return Color.PINK; //Oui, je sais pink = rose != violet... mais voilà quoi ! ^^
		case jaune : return Color.YELLOW;
		case vert : return Color.GREEN;
		case rouge : return Color.RED;
		default	: return null;
		}
	}
}