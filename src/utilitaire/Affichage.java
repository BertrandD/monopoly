package utilitaire;


import javax.swing.JOptionPane;

/**
 * 
 * TODO : prendre en compte l'affichage ou non de l'UI pour éviter le "mix" quand on lance le main sans IHM
 * @author Bertrand
 *
 */
public class Affichage {

	
	/**
	 * Méthode pour pouvoir afficher un message quelconque dans la console
	 * @author Bertrand
	 * @param s : le message à afficher
	 */
	public static void Afficher(String s){
		System.out.println(s);
	}

	/**
	 * Méthode pour pouvoir afficher un message de confirmation
	 * @author Bertrand
	 * @param s : le message ) afficher
	 */
	public static void AfficherConfirmation(String s){
		Colors.printColor(s, Colors.Vert);
		JOptionPane.showMessageDialog(null, s);
	}

	/**
	 * Méthode pour pouvoir afficher une question.
	 * @author Bertrand
	 * @param s : le message à afficher
	 */
	public static void AfficherQuestion(String s){
		Colors.printColor(s, Colors.Bleu);
	}

	/**
	 * Méthode pour poser une question oui/non au joueur et recevoir sa réponse en boolean.
	 * @author Bertrand
	 * @param s : le message à afficher
	 */
	public static boolean PoserQuestionOuiNon(String s){
        boolean res = false;

        if (s != null) {
            String [] choix = new String[] { "Oui", "Non" };

            Object selectedValue = JOptionPane.showOptionDialog(null,
                  s,
                  s,
                  JOptionPane.DEFAULT_OPTION,
                  JOptionPane.QUESTION_MESSAGE,
                  null,
                  choix,
                  choix[1]);
            if(((Integer) selectedValue) == JOptionPane.CLOSED_OPTION){
				boolean quitter = PoserQuestionOuiNon("Êtes-vous sûr de vouloir quitter le jeu ?");
					if(quitter){
						System.exit(0);
					}else{
						PoserQuestionOuiNon(s);
					}
            }
            res = (((Integer) selectedValue) == 0);
        }

        return res;
	}
	
	/**
	 * Méthode pour demander au joueur de faire un choix dans une liste donnée
	 * @author Bertrand
	 * @param s : le message à afficher
	 * @param choix : les différents choix disponibles
	 */
	public static String FaireChoixDansListe(String s, Object[] choix){
        String res = "";

        if (s != null) {

            Object selectedValue = JOptionPane.showInputDialog(null, s, "message", JOptionPane.DEFAULT_OPTION, null, choix, null);

            res = (String) selectedValue;
        }

        return res;
	}

	/**
	 * Méthode pour poser demander au joueur une valeur
	 * @author Bertrand
	 * @param s : le message à afficher
	 */
	public static int DemanderValeur(String s){
        int res = 0;

        if (s != null) {
            boolean ok = false;
            do{	
        		try{
        			Object selectedValue = JOptionPane.showInputDialog(null, s, s, JOptionPane.DEFAULT_OPTION, null, null, null);
        			if(selectedValue!=null){
            			res = Integer.parseInt(selectedValue.toString());
    	            	ok=true;
        			}else{
        				boolean quitter = PoserQuestionOuiNon("Êtes-vous sûr de vouloir quitter le jeu ?");
    						if(quitter){
    							System.exit(0);
    						}
        			}
	            }catch(Exception e){
	            	AfficherErreur("Merci de donner un nombre");
	            }
        	}while(!ok);
        }

        return res;
	}
	
	/**
	 * Méthode pour poser demander au joueur une valeur
	 * @author Bertrand
	 * @param s : le message à afficher
	 */
	public static String DemanderNom(String s){
        String res = "";

        if (s != null) {
            boolean ok = false;
            do{	
        		try{
        			Object selectedValue = JOptionPane.showInputDialog(null, s, s, JOptionPane.DEFAULT_OPTION, null, null, null);
        			if(selectedValue!=null){
        				res = selectedValue.toString();
    	            	ok=true;
        			}else{
        				boolean quitter = PoserQuestionOuiNon("Êtes-vous sûr de vouloir quitter le jeu ?");
    						if(quitter){
    							System.exit(0);
    						}
        			}
	            }catch(Exception e){
	            	AfficherErreur("Merci de donner une chaine de caractère valide");
	            }
        	}while(!ok);
        }

        return res;
	}	
	
	
	/**
	 * Méthode pour pouvoir afficher un message d'erreur.
	 * @author Bertrand
	 * @param s : le message à afficher
	 */
	public static void AfficherErreur(String s){
		Colors.printColor(s, Colors.Rouge);
		JOptionPane.showMessageDialog(null, "<html><font color='red'>"+s+"</font></html>");
	}
	


}
