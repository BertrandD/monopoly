package utilitaire;
public enum Famille {
	Chance, Communaute;

	public static Famille getFromLabel(String label){
		if(label.matches("chance")){
			return Chance;
		}else if(label.matches("communauté")){
			return Communaute;
		}else{
			return null;
		}
	}
}