package utilitaire;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.HashMap;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;

import jeu.Joueur;
import jeu.Monopoly;
/**
 * UI montrant les stats des différents joueurs (cash, propriétés...) et proposant des bontons d'action aux joueur
 * @author Bertrand
 *
 */
public class JoueursUI extends JFrame {

	private static final long serialVersionUID = -6863603357260233177L;
	private Monopoly monopoly;
	private HashMap<Joueur,JPanel> cases;
    final private ImageIcon icone = new ImageIcon(System.getProperty("user.dir")+"/icone.png");

	/**
	 * 
	 * @author Bertrand
	 * @param monopoly
	 */
	public JoueursUI(Monopoly monopoly){
		setMonopoly(monopoly);
		setCases(new HashMap<Joueur,JPanel>());
		try{
	    	UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
	    }catch(Exception e){}
		initComponents();
	}
	
	/**
	 * 
	 * @author Bertrand
	 */
	private void initComponents() {
    	this.setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridheight = 1;
        gbc.gridwidth = 1;
        gbc.gridx=0;
        gbc.gridy=0;
        for(Joueur j : getMonopoly().getJoueurs()){
            addCell(gbc,j);
        	gbc.gridy++;
        }
	}
	
	/**
	 * 
	 * @author Bertrand
	 * @param gbc
	 * @param j
	 */
    private void addCell(GridBagConstraints gbc, Joueur j){
    	JPanel cell1 = new JPanel();
    	cell1.setBorder(BorderFactory.createLineBorder(Color.black));
//        cell1.setPreferredSize(new Dimension(290, 165));
        JLabel label=new JLabel(getTexteCase(j));
        cell1.add(label);
        JButton jouerUnCoup = new JButton("Jouer un coup");
        jouerUnCoup.setEnabled(false);
        JButton construire = new JButton("Constuire");
        construire.setEnabled(false);
        JButton finDuTour = new JButton("Fin du tour");
        finDuTour.setEnabled(false);
        cell1.add(jouerUnCoup);
        cell1.add(construire);
        cell1.add(finDuTour);
        getCases().put(j,cell1);
        this.add(cell1, gbc);
    }
    
    @Override
    public void repaint(){
    	JPanel jp;
    	for(Joueur jo : getCases().keySet()){
    		jp = getCases().get(jo);
    		((JLabel) jp.getComponent(0)).setText(getTexteCase(jo));
    	}
    	this.pack();
    }

    /**
     * Rend visible la fenêtre
     * @author Bertrand
     */
    public void afficher() {
    	this.setIconImage(getIcone().getImage());
        this.setDefaultCloseOperation(javax.swing.JFrame.EXIT_ON_CLOSE);
        this.setLocation(1130, 0);
        this.setTitle("Joueurs");
        this.setSize(300, 990);
        this.setResizable(false);
        this.setFocusable(true);
        this.pack();
        this.setVisible(true);                        
    }
   
    /**
     * Donne le texte à afficher dans la case du joueur
     * @author Bertrand
     * @param j
     * @return
     */
    private String getTexteCase(Joueur j){
    	String t= "";
    	if(j.enPrison()){
    		t += "<font color='red'> -- En prison --</font>";
    	}
    	if(j.getCash()<0){
    		t += "<font color='red'> -- Eliminé --</font>";
    	}else{
    		t += "<br/>Cash :"+j.getCash()+" €</center><br/>";
    	}
    	return "<html><div width=290><center>"+j.getNomJoueur()+t
        		+getMonopoly().afficherProprietes(j)
        		+"</div></html>";
    }
    
    /**
     * Pour accéder à la case liée à un joueur
     * @author Bertrand
     * @return
     */
    public HashMap<Joueur,JPanel> getCases(){
    	return cases;
    }
    
    private void setCases(HashMap<Joueur,JPanel> c){
    	this.cases = c;
    }
    
    private Monopoly getMonopoly(){
    	return monopoly;
    }

	private void setMonopoly(Monopoly m){
    	this.monopoly = m;
    } 
	
	public ImageIcon getIcone() {
		return icone;
	}

}
