package utilitaire;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.UIManager;

import data.Carreau;
import data.CarreauArgent;
import data.CarreauPropriete;
import data.ProprieteAConstruire;
import jeu.Joueur;
import jeu.Monopoly;
/**
 * UI du plateau de jeu
 * @author darbonb
 *
 */
public class FenetreUI extends JFrame{

	private static final long serialVersionUID = 3555733796586567682L;
	private JMenuItem [] menuFichier;


	private Monopoly monopoly;
	private HashMap<Carreau, JPanel> cases;
    final private ImageIcon backgroundImage = new ImageIcon(System.getProperty("user.dir")+"/monopoly.png");
    final private ImageIcon icone = new ImageIcon(System.getProperty("user.dir")+"/icone.jpg");
	
	public ImageIcon getIcone() {
		return icone;
	}

	/**
	 * 
	 * @author Bertrand
	 * @param monopoly
	 */
	public FenetreUI(Monopoly monopoly){
		setMonopoly(monopoly);
		setMenuFichier(new JMenuItem[4]);
        setCases(new HashMap<Carreau, JPanel>());
		try{
	    	UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
	    }catch(Exception e){}
		initMenus();
		initComponents();
	}
	
	/**
	 * 
	 * @author Bertrand
	 */
    private void initComponents() {
    	this.setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridheight = 1;
        gbc.gridwidth = 1;
        int k=1;
        for(int y=0;y<11;y++){
        	if(y==0){
            	for(int x=0;x<11;x++){
                    gbc.gridx=x;
                    gbc.gridy=y;
                    addCell(gbc,k);
                    k++;
            	}
        	}else if(y==10){
            	for(int x=10;x>=0;x--){
                    gbc.gridx=x;
                    gbc.gridy=y;
                    addCell(gbc,k);
                    k++;
            	}
        	}else{
        		gbc.gridx=10;
                gbc.gridy=y;
                addCell(gbc,k);
                k++;
        	}
        }
        for(int y=9;y>0;y--){
    		gbc.gridx=0;
            gbc.gridy=y;
            addCell(gbc,k);
            k++;
        }
        gbc.gridx=1;
        gbc.gridy=1;
        gbc.gridheight=9;
        gbc.gridwidth=9;
        
        JLabel t = new JLabel(getBackgroundImage());
        this.add(t,gbc);
    }
    
    /**
     * 
     * @author Bertrand
     * @param gbc
     * @param k
     */
    private void addCell(GridBagConstraints gbc, int k){
    	JPanel cell1 = new JPanel();
    	cell1.setBorder(BorderFactory.createLineBorder(Color.black));
        cell1.setPreferredSize(new Dimension(100, 80));
        Carreau c = getMonopoly().getCarreau(k);
        cell1.add(new JLabel(getTexteCase(c)));
        if(c instanceof ProprieteAConstruire){
        	cell1.setBackground(((ProprieteAConstruire) c).getGroupe().getCouleur().getColor());
        }
        this.add(cell1, gbc);
        getCases().put(c,cell1);
    }
    
    @Override
    public void repaint(){
    	JPanel Jp;
    	JLabel Jl;
    	for(Carreau c : getCases().keySet()){
    		Jp = getCases().get(c);
    		((JLabel) Jp.getComponent(0)).setText(getTexteCase(c));
    	}
    	for(Joueur j : getMonopoly().getJoueurs()){
    		Jp=getCases().get(j.getPositionCourante());
    		Jl =((JLabel) Jp.getComponent(0));
    		Jl.setText(Jl.getText()+"<b>"+j.getNomJoueur()+"</b> ");
    	}
    	super.repaint();
    	this.pack();
    }
    
    /**
     * Donne le texte à afficher pour le carreau
     * @author Bertrand
     * @param cell1
     * @param c
     * @return
     */
    public String getTexteCase(Carreau c){
    	String s="<html><div width=95><center>"+c.getNomCarreau()+"<br/>";
        if(c instanceof CarreauPropriete){
            s+=((CarreauPropriete) c).getPrixAchat()+" €<br/>";
        }else if(c instanceof CarreauArgent){
            s+=((CarreauArgent) c).getMontant()+" €<br/>";
        }
        return s;
    }
    
    /**
     * Initialise les menus
     * @author Bertrand
     */
	private void initMenus() {
        JMenuBar barreMenu = new JMenuBar();
        barreMenu.add(initMenuFichier());
        this.setJMenuBar(barreMenu);
    }

	/**
	 * Unique menu pour lancer une partie
	 * TODO : enregistrement des parties ?
	 * @author Bertrand
	 * @return
	 */
    private JMenu initMenuFichier() {
        JMenu menu;
                
        menu = new JMenu("Fichier");
                
        getMenuFichier()[0] = new JMenuItem("Nouvelle partie");
        getMenuFichier()[0].addActionListener(new ActionListener(){
                @Override
                public void actionPerformed(ActionEvent e) {
                	getMonopoly().nouvellePartie();
                }
            }
        );
        menu.add(getMenuFichier()[0]);
        
        getMenuFichier()[1] = new JMenuItem("Nouvelle partie (test)");
        getMenuFichier()[1].addActionListener(new ActionListener(){
                @Override
                public void actionPerformed(ActionEvent e) {
                	getMonopoly().initTest();
                	getMonopoly().nouvellePartie();
                }
            }
        );
        menu.add(getMenuFichier()[1]);
        
        getMenuFichier()[2] = new JMenuItem("Nouvelle partie (scenario)");
        getMenuFichier()[2].addActionListener(new ActionListener(){
                @Override
                public void actionPerformed(ActionEvent e) {
                	getMonopoly().initScenario();
                	getMonopoly().nouvellePartie();
                }
            }
        );
        menu.add(getMenuFichier()[2]);
        
        /* Quitter */
        getMenuFichier()[3] = new JMenuItem("Quitter");
        getMenuFichier()[3].addActionListener(new ActionListener(){
                @Override
                public void actionPerformed(ActionEvent e) {
                    boolean res = Affichage.PoserQuestionOuiNon("Êtes-vous sûr de vouloir quitter le jeu ?");
                    if(res){
                    	System.exit(0);
                    }
                }
            }
        );
        menu.add(getMenuFichier()[3]);
        
        return menu;
    }
    
    /**
     * Permet d'afficher la fenêtre
     * @author Bertrand
     */
    public void afficher() {
    	this.setIconImage(getIcone().getImage());
        this.setDefaultCloseOperation(javax.swing.JFrame.EXIT_ON_CLOSE);
        this.setTitle("Monopoly");
        this.setSize(1120, 990);
        this.setResizable(false);
        this.pack();
        this.setVisible(true);                        
    }

    /**
     * Pour accéder au JPanel lié à un carreau
     * @author Bertrand
     * @return
     */
	public HashMap<Carreau, JPanel> getCases() {
		return cases;
	}

	private void setCases(HashMap<Carreau, JPanel> cases) {
		this.cases = cases;
	}

	private Monopoly getMonopoly() {
		return monopoly;
	}

	private void setMonopoly(Monopoly monopoly) {
		this.monopoly = monopoly;
	}

	public ImageIcon getBackgroundImage() {
		return backgroundImage;
	}
	
	public JMenuItem[] getMenuFichier() {
		return menuFichier;
	}

	public void setMenuFichier(JMenuItem[] menuFichier) {
		this.menuFichier = menuFichier;
	}
}
