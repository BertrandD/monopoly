package utilitaire;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class BancDeTest {
	private ArrayList<int[]> listeLancers;
	private ArrayList<Integer> listeCartes;
	private int indexLancers;
	private int indexCartes;
	
	public BancDeTest(String dirDes, String dirCartes){
		setIndexLancers(0);
		setIndexCartes(0);
		setListeLancers(new ArrayList<int[]>());
		setListeCartes(new ArrayList<Integer>());
		try {
			creerListeCartes(dirCartes);
			creerListeLancers(dirDes);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private ArrayList<int[]> getListeLancers() {
		return listeLancers;
	}

	private void setListeLancers(ArrayList<int[]> listeLancers) {
		this.listeLancers = listeLancers;
	}

	private int getIndexLancers() {
		return indexLancers;
	}

	private void setIndexLancers(int index) {
		this.indexLancers = index;
	}
	
	private void creerListeLancers(String dir) throws FileNotFoundException, IOException{
		ArrayList<String[]> data = readDataFile(dir, ";");
		for(String[] s:data){
			int[] data2 = new int[2];
			data2[0] = Integer.parseInt(s[0]);
			data2[1] = Integer.parseInt(s[1]);
			this.getListeLancers().add(data2);
		}
	}
	
	private void creerListeCartes(String dir) throws FileNotFoundException, IOException{
		ArrayList<String[]> data = readDataFile(dir, ";");
		for(String[] s:data){
			System.out.println(s);
			int data2 = Integer.parseInt(s[0]);
			this.getListeCartes().add(data2);
		}
	}
	
	public int[] getLancer(){
		if(getIndexLancers()<getListeLancers().size()){
			setIndexLancers(getIndexLancers()+1);
		}else{
			int[] ret = {0,0};
			return ret;
		}
		return getListeLancers().get(getIndexLancers()-1);
	}
	
	public int getCarte(){
		if(getIndexCartes()<getListeCartes().size()){
			setIndexCartes(getIndexCartes()+1);
		}else{
			return 40;
		}
		return getListeCartes().get(getIndexCartes()-1);
	}
	
	private ArrayList<String[]> readDataFile(String filename, String token) throws FileNotFoundException, IOException{
		ArrayList<String[]> data = new ArrayList<String[]>();
		
		BufferedReader reader  = new BufferedReader(new FileReader(filename));
		String line = null;
		while((line = reader.readLine()) != null){
			data.add(line.split(token));
		}
		reader.close();
		
		return data;
	}

	private ArrayList<Integer> getListeCartes() {
		return listeCartes;
	}

	private void setListeCartes(ArrayList<Integer> listeCartes) {
		this.listeCartes = listeCartes;
	}

	private int getIndexCartes() {
		return indexCartes;
	}

	private void setIndexCartes(int indexCartes) {
		this.indexCartes = indexCartes;
	}

}
