package utilitaire;

/**
 * 
 * @author Bertrand Darbon
 *
 */
public enum Colors {
	Bleu,
	Rouge,
	Vert,
	Blanc;
	
	@Override
	public String toString(){
		String couleur = "";
		
		couleur=this.getCode();
		
		
		return"\033["+ couleur +"m"+super.toString()+"\033[0m";
	}
	
	public static void printColor(String txt, Colors color){
		printColor(txt,color,true);
	}
	
	public static void printColor(String txt, Colors color, boolean ln){
		if (ln) {
			System.out.println("\033["+ color.getCode() +"m"+txt+"\033[0m");
		}else{
			System.out.print("\033["+ color.getCode() +"m"+txt+"\033[0m");
		}
	}
	
	public String getCode(){
		switch(this){
		case Bleu : return "34";
		case Rouge : return "31";
		case Vert : return "32";
		case Blanc : return "37";
		default	: return "";
		}
	}
}
