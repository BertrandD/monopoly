package data;

import jeu.Joueur;
import jeu.Monopoly;

public class ProprieteAConstruire extends CarreauPropriete implements Comparable<Object>{
	
	// Attributs
	private Groupe groupePropriete;
	private int nbMaisons;
	private boolean hasHotel;
	private int[] loyers;
	
	// Constructeurs
	public ProprieteAConstruire(int num, String nomC, Monopoly mono, int prixA,int[] loyer,Groupe grp){
		super(num, nomC, mono, prixA);
		this.setGroupePropriete(grp);
		this.setLoyers(loyer);
	}
	
	// Getters & Setters
	private void setNbMaisons(int nbMaisons) {
		this.nbMaisons = nbMaisons;
	}
	
	public Groupe getGroupe() {
		return groupePropriete;
	}
	
	public int getNbMaisons() {
		return this.nbMaisons;
	}
	
	private void setGroupePropriete(Groupe groupePropriete) {
		this.groupePropriete = groupePropriete;
		groupePropriete.ajouterProprieteAConstruire(this);
	}
	
	private void setHasHotel(boolean hasHotel) {
		this.hasHotel = hasHotel;
	}
	
	private void setLoyers(int[] loyers) {
		this.loyers = loyers;
	}
	
	public int[] getLoyers() {
		return loyers;
	}
	
	public boolean isHotel() {
		return hasHotel;
	}
	
	@Override
	public void viderPropriete(){
		this.setProprietaire(null);
		this.setNbMaisons(0);
		this.setHasHotel(false);
	}
	
	// Methods
	/**
	 * Le choisi ou non de constuire une maison sur cette propriété.
	 * @author lebocp
	 * @param j : le joueur qui effectue l'action
	 */
	@Override
	public void action(Joueur j) {
		super.action(j);
//		if(getNbMaisons() < 4)
//		{
//			if(Affichage.PoserQuestionOuiNon("Désirez-vous construire une maison sur "+getNomCarreau()+" ?"))
//			{
//				int montant = getGroupe().getPrixAchatMaison();
//				if(j.getCash() >= montant)
//				{
//					j.payer(montant);
//					AjouterMaison();
//				}
//				else
//				{
//					Affichage.Afficher("Vous n'avez pas le cash suffisant pour construire une maison !");
//				}
//			}
//		}
//		else
//		{
//			if(Affichage.PoserQuestionOuiNon("Désirez-vous construire un hotel sur "+getNomCarreau()+" ?"))
//			{
//				int montant = getGroupe().getPrixAchatHotel();
//				if(j.getCash() >= montant)
//				{
//					j.payer(montant);
//					construireHotel();
//				}
//				else
//				{
//					Affichage.Afficher("Vous n'avez pas le cash suffisant pour construire un hotel !");
//				}
//			}
//		}
	}
	
	public int calculLoyer(Joueur j) {
		if(this.isHotel()){
			return this.getLoyers()[5];
		}else{
			return this.getLoyers()[this.getNbMaisons()];
		}
	}
	
	public void ajouterMaison() {
		this.setNbMaisons(this.getNbMaisons()+1);
	}
	
	public void construireHotel() {
		this.setHasHotel(true);
	}

	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Object o) {
		int nb1 = ((ProprieteAConstruire) o).getNumero();
		int nb2 = this.getNumero();
		if(nb1>nb2) return -1;
		else if(nb1==nb2) return 0;
		else return 1;
	}

	/**
	 * @author Bertrand
	 * @param i
	 */
	public void retirerMaisons(int i) {
		setNbMaisons(getNbMaisons()-i);
	}
}