package data;

import utilitaire.Affichage;
import jeu.Joueur;
import jeu.Monopoly;

public class Compagnie extends CarreauPropriete {

	// Attributs
	private int[] multiple = {4,10};
	
	// Constructeurs
	public Compagnie(int num, String nomC, Monopoly mono, int prixA) {
		super(num, nomC, mono, prixA);
	}
	
	// Getters & Setters
	public int[] getMultiple() {
		return multiple;
	}

	public void setMultiple(int[] multiple) {
		this.multiple = multiple;
	}
	
	// Methods
	/**
	 * @param j
	 */
	public void action(Joueur j) {
		super.action(j);
	}
	
	/**
	 * Le loyer est calculé en fonction du nombre de compagnies<br>
	 * 1 compagnie : loyer = 4 x résultat des dés<br>
	 * 2 compagnies : loyer = 10 x résultat des dés<br>
	 * @author lebocp
	 * @since 05/01/2015
	 * @return le loyer à payer
	 */
	@Override
	public int calculLoyer(Joueur j) {
		Joueur proprietaire = getProprietaire();
		int nbCompagnies = proprietaire.getNbCompagnies();
		int loyer = 0;
		
		// On ne fait pas relancer les dés... s'il est arrivé sur la compagnie, il a forcément tiré des dés... La téléportation n'existe pas dans le Monopoly....
		if(j.getLancers()[0] == 0 && j.getLancers()[1] == 0){
			Affichage.AfficherConfirmation(j.getNomJoueur()+" tire les dés pour déterminer le loyer");
			j.lancerLesDes();
		}
		
		loyer = (j.getLancers()[0]+j.getLancers()[1])*this.getMultiple()[nbCompagnies-1];
		
		return loyer;
	}
}