package data;
import jeu.Joueur;
import jeu.Monopoly;

public class Gare extends CarreauPropriete {

	public Gare(int num, String nomC, Monopoly mono, int prixA) {
		super(num, nomC, mono, prixA);
	}

	/**
	 * @param j
	 */
	@Override
	public void action(Joueur j) {
		super.action(j);
	}

	/**
	 * Si le proprietaire possède :<br>
	 * 1 Gare : 25€<br>
	 * 2 Gares : 50€<br>
	 * 3 Gares : 75€<br>
	 * 4 Gares : 100€<br>
	 * 
	 * La formule de la methode est donc => Nombre de gares * 25€
	 * @author lebocp
	 * @since 05/01/2015
	 */
	@Override
	public int calculLoyer(Joueur j) {
		return 25 * getProprietaire().getNbGares();
	}
}