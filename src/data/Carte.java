package data;

import jeu.Joueur;
import jeu.Monopoly;
import utilitaire.Famille;

public abstract class Carte {
	
	// Attributs
	private Monopoly monopoly;
	private Famille type;
	private String texte;
	private int numero;
	
	// Constructeurs
	public Carte(Monopoly mono, Famille type, String texte){
		this.setMonopoly(mono);
		this.setType(type);
		this.setTexte(texte);
	}
	
	// Getters & Setters
	public Monopoly getMonopoly() {
		return monopoly;
	}

	private void setMonopoly(Monopoly monopoly) {
		this.monopoly = monopoly;
	}

	public Famille getType() {
		return type;
	}

	private void setType(Famille type) {
		this.type = type;
	}

	public String getTexte() {
		return texte;
	}

	private void setTexte(String texte) {
		this.texte = texte;
	}
	
	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	// Methods
	public abstract void action(Joueur j);
}