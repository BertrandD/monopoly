package data;

import jeu.Joueur;
import jeu.Monopoly;
import utilitaire.Famille;

public class CarteArgent extends Carte {
	
	// Attributs
	private int montant;
	
	// Constructeurs
	public CarteArgent(Monopoly mono, Famille type, String texte, int montant) {
		super(mono, type, texte);
		setMontant(montant);
	}
	
	// Getters & Setters
	public int getMontant() {
		return montant;
	}
	
	private void setMontant(int montant) {
		this.montant = montant;
	}
	
	/**
	 * @author lebocp
	 * @since 06/01/2015
	 */
	@Override
	public void action(Joueur j) {
		if(getMontant() > 0){
			j.recevoir(getMontant());
		}else{
			// La donnée montant est un nombre négatif lors qu'il faut payer et non recevoir.
			j.payer(Math.abs(getMontant()));
		}
	}

}