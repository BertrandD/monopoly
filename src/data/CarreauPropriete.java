package data;

import jeu.Joueur;
import jeu.Monopoly;
import utilitaire.Affichage;

public abstract class CarreauPropriete extends Carreau {

	// Attributs
	private Joueur proprietaire;
	private int prixAchat;
	private int loyerBase;

	// Constructeurs
	/**
	 * 
	 * @author Bertrand
	 * @param num
	 * @param nomC
	 * @param mono
	 * @param prixA
	 */
	public CarreauPropriete(int num, String nomC, Monopoly mono, int prixA) {
		super(num, nomC, mono);
		setPrixachat(prixA);
		setProprietaire(null);
	}
	
	private void setPrixachat(int prixA) {
		this.prixAchat = prixA;
	}

	public int getPrixAchat() {
		return this.prixAchat;
	}
	
	public Joueur getProprietaire() {
		return proprietaire;
	}
	
	protected void setProprietaire(Joueur j) {
		this.proprietaire = j;
	}
	
	public int getLoyerBase() {
		return loyerBase;
	}

	public void setLoyerBase(int loyerBase) {
		this.loyerBase = loyerBase;
	}
	
	// Methods
	/**
	 * Vérifie si la propriété n'appartient à personne et propose donc la mise en vente.
	 * Dans le cas contraire, elle fait payer le loyer au joueur
	 * Méthode identique à tout les type de propriétés
	 * @param j le joueur qui vient d'arriver sur la case
	 */
	@Override
	public void action(Joueur j) {
		if(getProprietaire() != null){
			if(!getProprietaire().equals(j)){
				int loyer = calculLoyer(j);
				int l = j.payer(loyer);
				getProprietaire().recevoir(l);
				Affichage.AfficherConfirmation(j.getNomJoueur()+" est arrivé sur "+getNomCarreau()+" et a versé le loyer s'élevant à "+l+"€ à "+getProprietaire().getNomJoueur());
			}
		}else{
			if(j.getCash() >= getPrixAchat()){
				Monopoly m = getMonopoly();
				Boolean choix = m.afficherMessage(j, this);
				if(choix){
					j.payer(getPrixAchat());
					if(this instanceof ProprieteAConstruire){
						j.ajouterProprieteAConstruire((ProprieteAConstruire) this);
					}else if(this instanceof Compagnie){
						j.ajouterCompagnie((Compagnie) this);
					}else if(this instanceof Gare){
						j.ajouterGare((Gare) this);
					}
					setProprietaire(j);
					Affichage.AfficherConfirmation("Vous avez bien acheté "+getNomCarreau()+"\n Cash : "+j.getCash());					
				}
			}else{
				Affichage.AfficherConfirmation("Vous n'avez pas assez d'argent pour achetter cette propriété");
			}
		}
	}
	
	protected abstract int calculLoyer(Joueur j);
	
	public void viderPropriete() {
		this.setProprietaire(null);
	}
}