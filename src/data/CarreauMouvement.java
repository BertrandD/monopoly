package data;
import utilitaire.Affichage;
import jeu.Joueur;
import jeu.Monopoly;

public class CarreauMouvement extends CarreauAction {

	public CarreauMouvement(int num, String nomC, Monopoly mono) {
		super(num, nomC, mono);
	}

	/**
	 * Envoi le joueur en prison et affiche un message.<br>
	 * <b>(Le seul CarreauMouvement étant la case "Allez en prison")</b>
	 * @param j : le joueur à envoyer en prison.
	 */
	public void action(Joueur j) {
		Affichage.AfficherConfirmation(j.getNomJoueur()+" va en prison.");
		Carreau carreau = super.getMonopoly().getCarreau(11);
		j.setPositionCourante(carreau);
		j.setPrison(true);
	}
}