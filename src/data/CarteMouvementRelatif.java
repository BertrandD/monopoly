package data;

import jeu.Joueur;
import jeu.Monopoly;
import utilitaire.Famille;

public class CarteMouvementRelatif extends Carte {
	
	// Attributs
	private int mouvement;
	
	// Constructeurs
	public CarteMouvementRelatif(Monopoly mono, Famille type, String texte, int mouvement) {
		super(mono, type, texte);
		setMouvement(mouvement);
	}
	
	// Getters & Setters
	public int getMouvement() {
		return mouvement;
	}
	
	public void setMouvement(int mouvement) {
		this.mouvement = mouvement;
	}
	
	// Methods
	/**
	 * @author lebocp
	 * @since 06/01/2015
	 * Nouvelle position = position courante + nombre de case à avancer.
	 */
	@Override
	public void action(Joueur j) {
		Carreau c = getMonopoly().getCarreau(j.getPositionCourante().getNumero()+getMouvement());
		j.setPositionCourante(c);
		c.action(j);
		int[] l = {0,0};
		j.setLancers(l);
	}
}