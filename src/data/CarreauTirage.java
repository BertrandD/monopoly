package data;
import jeu.Joueur;
import jeu.Monopoly;
import utilitaire.Affichage;
import utilitaire.Famille;

public class CarreauTirage extends CarreauAction {

	// Attributs
	private Famille type;
	
	// Constructeurs
	public CarreauTirage(int num, String nomC, Monopoly mono) {
		super(num, nomC, mono);
		setType(nomC);
	}
	
	// Getters & Setters
	public Famille gettype() {
		return this.type;
	}
	
	private void setType(String famille) {
		type = Famille.getFromLabel(famille); 
	}

	// Methods
	@Override
	public void action(Joueur j) {
		Carte carte = getMonopoly().tirerCarte(gettype());
		
		if(carte instanceof CarteSortirPrison){
			j.ajouterCarteSortirPrison(carte);
			getMonopoly().retirerCarteSortirPrison(carte);
		}else{
			carte.action(j);
		}
		Affichage.AfficherConfirmation(j.getNomJoueur()+" : "+carte.getTexte());
	}

}