package data;

import jeu.Joueur;
import jeu.Monopoly;
import utilitaire.Famille;

public class CarteMouvementAbsolu extends Carte {
	
	// Attributs
	private Carreau carreau;
	private boolean franchirCaseDepart;
	
	// Contructeurs
	public CarteMouvementAbsolu(Monopoly mono, Famille type, String texte, Carreau mouvement, boolean franchirDepart) {
		super(mono, type, texte);
		setNouveauCarreau(mouvement);
		setFranchirCaseDepart(franchirDepart);
	}
	
	// Getters & Setters
	public boolean getFranchirCaseDepart() {
		return franchirCaseDepart;
	}
	
	private void setFranchirCaseDepart(boolean caseDepart) {
		this.franchirCaseDepart = caseDepart;
	}
	
	public Carreau getNouveauCarreau() {
		return carreau;
	}
	
	private void setNouveauCarreau(Carreau carreau) {
		this.carreau = carreau;
	}
	
	// Methods
	/**
	 * @author lebocp
	 * @since 06/01/2015
	 */
	@Override
	public void action(Joueur j) {
		if(getFranchirCaseDepart())
		{
			int id = getNouveauCarreau().getNumero();
			int currentPos = j.getPositionCourante().getNumero();
			
			if(id < currentPos)
			{
				j.recevoir(200);
			}
		}
		j.setPositionCourante(getNouveauCarreau());
		if(getNouveauCarreau().getNumero() == 11){
			j.setPrison(true);
		}
	}
}