package data;

import jeu.Joueur;
import jeu.Monopoly;
import utilitaire.Famille;

public class CarteSortirPrison extends Carte {

	// Contructeurs
	public CarteSortirPrison(Monopoly mono, Famille type, String texte) {
		super(mono, type, texte);
	}
	
	// Methods
	@Override
	public void action(Joueur j) {
		j.ajouterCarteSortirPrison(this);
	}
}