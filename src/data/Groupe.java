package data;
import java.util.*;

import utilitaire.CouleurPropriete;

public class Groupe {

	public static ArrayList<Groupe> groupes = new ArrayList<>();
	private ArrayList<ProprieteAConstruire> proprietes;
	private CouleurPropriete couleur;
	private int prixAchatMaison;
	private int prixAchatHotel;
	
	// Constructeurs
	public Groupe(CouleurPropriete couleur, int prixAchatMaison, int prixAchatHotel){
		setCouleur(couleur);
		setPrixAchatMaison(prixAchatMaison);
		setPrixAchatHotel(prixAchatHotel);
		setProprietes(new ArrayList<ProprieteAConstruire>());
		getTousLesGroupes().add(this);
	}
	
	// Methods
	
	/**
	 * Ajoute une propriété dans le groupe après avoir check qu'elle n'est pas déjà présente.
	 * La methode 
	 * @param propriete
	 */
	public void ajouterProprieteAConstruire(ProprieteAConstruire propriete){
		int compteur = 0;
		boolean dejaPresent = false;
		
		while(!dejaPresent && compteur < getProprietes().size()){
			if(getProprietes().get(compteur).equals(propriete))
				dejaPresent = true;
			compteur++;
		}
		
		if(!dejaPresent)
			addProprieteAConstruire(propriete);
	}
	
	/**
	 * Ajout de la propriete dans le groupe.
	 * @param propriete
	 */
	private void addProprieteAConstruire(ProprieteAConstruire propriete) {
		getProprietes().add(propriete);
	}
	
	

	// Getters & Setters
	
	private void setPrixAchatMaison(int prixAchatMaison) {
		this.prixAchatMaison = prixAchatMaison;
	}

	private void setPrixAchatHotel(int prixAchatHotel) {
		this.prixAchatHotel = prixAchatHotel;
	}

	private void setProprietes(ArrayList<ProprieteAConstruire> p) {
		this.proprietes = p;
	}


	public ArrayList<ProprieteAConstruire> getProprietes() {
		return proprietes;
	}


	public int getPrixAchatMaison() {
		return this.prixAchatMaison;
	}

	public int getPrixAchatHotel() {
		return this.prixAchatHotel;
	}

	public CouleurPropriete getCouleur() {
		return couleur;
	}

	public void setCouleur(CouleurPropriete couleur) {
		this.couleur = couleur;
	}
	
	public static ArrayList<Groupe> getTousLesGroupes(){
		return groupes;
	}
}