package data;
import utilitaire.Affichage;
import jeu.Joueur;
import jeu.Monopoly;

public class CarreauArgent extends CarreauAction {

	// Attributs
	private int montant;
	
	// Constructeurs
	/**
	 * Carreau destiné a gérer de l'argent
	 * @param num
	 * @param nomC
	 * @param mono
	 * @param montant
	 */
	public CarreauArgent(int num, String nomC, Monopoly mono, int montant) {
		super(num, nomC, mono);
		setMontant(montant);
	}
	
	// Getters & Setters
	public int getMontant() {
		return montant;
	}
	
	private void setMontant(int montant) {
		this.montant = montant;
	}
	
	// Methods
	@Override
	public void action(Joueur j) {
		if(montant<0){
			j.payer(Math.abs(montant));
			Affichage.AfficherConfirmation(j.getNomJoueur()+" a payé "+Math.abs(montant)+"€");
		}else if (montant>0){
			j.recevoir(montant);
			Affichage.AfficherConfirmation(j.getNomJoueur()+" a gagné "+montant+"€");
		}
	}
}