package data;

import jeu.Joueur;
import jeu.Monopoly;
import utilitaire.Famille;

public class CarteAnniversaire extends Carte {
	
	// Attributs
	private int montant;
	
	// Constructeurs
	public CarteAnniversaire(Monopoly mono, Famille type, String texte, int montant) {
		super(mono, type, texte);
		setMontant(montant);
	}
	
	// Getters & Setters
	public int getMontant() {
		return montant;
	}
	
	private void setMontant(int montant) {
		this.montant = montant;
	}
	
	// Method
	/**
	 * @author lebocp
	 * @since 06/01/2014
	 * C'est votre anniversaire ! Tous les joueurs vous donnent getMontant() !
	 */
	public void action(Joueur j) {
		int montant = getMontant();
		
		for(Joueur autreJoueur : getMonopoly().getJoueurs())
		{
			if(!autreJoueur.equals(j))
			{
				autreJoueur.payer(montant);
				j.recevoir(montant);
			}
		}
	}
}