package data;

import jeu.Joueur;
import jeu.Monopoly;
import utilitaire.Famille;

public class CarteReparations extends Carte {
	
	// Attributs
	private int montantMaison;
	private int montantHotel;
	
	// Constructeurs
	public CarteReparations(Monopoly monopoly, Famille type, String texte, int montantMaison, int montantHotel) {
		super(monopoly, type, texte);
		setMontantMaison(montantMaison);
		setMontantHotel(montantHotel);
	}
	
	//Getters&Setters
	private void setMontantMaison(int montantMaison) {
		this.montantMaison=montantMaison;
	}
	
	private void setMontantHotel(int montantHotel) {
		this.montantHotel=montantHotel;
	}
	
	public int getMontantMaison() {
		return this.montantMaison;
	}
	
	public int getMontantHotel() {
		return this.montantHotel;
	}
	
	// Methods
	/**
	 * Action : Paie le montant total des réparation en fonction du nombre de maison et d'hotel.
	 * @author lebocp
	 * @since 06/01/2015
	 * @param j : le joueur qui effectue l'action.
	 */
	@Override
	public void action(Joueur j) {
		int montant = calculerPrixReparations(j.getNbMaisons(), j.getNbHotels());
		j.payer(montant);
	}
	
	/**
	 * Pour être en accord avec le diagramme de séquence
	 * @author Bertrand
	 * @param nbM
	 * @param nbH
	 * @return
	 */
	private int calculerPrixReparations(int nbM, int nbH){
		return (nbM*getMontantMaison())+(nbH*getMontantHotel());
	}
}