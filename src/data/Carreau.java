package data;

import jeu.Joueur;
import jeu.Monopoly;

public abstract class Carreau {

	// Attributs
	private Monopoly monopoly;
	private int numero;
	private String nomCarreau;
	
	// Constructeurs
	public Carreau(int num, String nomC, Monopoly mono){
		setNumero(num);
		setNomCarreau(nomC);
		setMonopoly(mono);		
	}
	
	// Getters & Setters
	private void setMonopoly(Monopoly mono) {
		this.monopoly = mono;
	}
	
	private void setNomCarreau(String nomC) {
		this.nomCarreau = nomC;
	}

	private void setNumero(int num) {
		this.numero = num;
	}

	public int getNumero() {
		return this.numero;
	}

	public String getNomCarreau() {
		return nomCarreau;
	}

	public Monopoly getMonopoly() {
		return this.monopoly;
	}

	// Methods
	/**
	 * @param j : le joueur qui effectue l'action
	 */
	public abstract void action(Joueur j);
}