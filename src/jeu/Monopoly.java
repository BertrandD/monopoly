package jeu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

import javax.swing.JButton;

import data.Carreau;
import data.CarreauArgent;
import data.CarreauMouvement;
import data.CarreauPropriete;
import data.CarreauTirage;
import data.Carte;
import data.CarteAnniversaire;
import data.CarteArgent;
import data.CarteMouvementAbsolu;
import data.CarteMouvementRelatif;
import data.CarteReparations;
import data.CarteSortirPrison;
import data.Compagnie;
import data.Gare;
import data.Groupe;
import data.ProprieteAConstruire;
import utilitaire.Affichage;
import utilitaire.BancDeTest;
import utilitaire.CouleurPropriete;
import utilitaire.Famille;
import utilitaire.FenetreUI;
import utilitaire.JoueursUI;

public class Monopoly {

	// Attributs
	private ArrayList<Carreau> carreaux;
	private ArrayList<Joueur> joueurs;
	private HashMap<Integer,Carte> cartes;
	private int nbMaisons = 32;
	private int nbHotels = 12;
	private BancDeTest test;
	private FenetreUI fenetreUI;
	private boolean UI;
	private Joueur prochainJ;
	private JoueursUI joueursUI;
	
	// Constructeurs
	/**
	 * Constructeur de Monopoly : Instancie un monopoly
	 * @param dataFilename
	 */
	public Monopoly(String dataFilename){
		init(dataFilename);
		setUI(false);
		setTest(null);
	}

	/**
	 * Constructeur de Monopoly : Instancie un monopoly
	 * @author Bertrand
	 * @param dataFilename
	 * @param ui
	 */
	public Monopoly(String dataFilename, boolean ui){
		init(dataFilename);
		setUI(ui);
		if(ui)
			initUI();
	}
	
	/**
	 * initialisation du monopoly
	 * 
	 */
	private void init(String dataFilename){
		setNbMaisons(nbMaisons);
		setNbHotels(nbHotels);
		
		setCarreaux(new ArrayList<Carreau>());
		setJoueurs(new ArrayList<Joueur>());
		setCartes(new HashMap<Integer, Carte>());
		
		buildGamePlateau(dataFilename);
		buildGameCartes(System.getProperty("user.dir")+"/src/data/cartes.data"); 
		setTest(null);
	}
	
	// Getters & Setters
	public void setCartes(HashMap<Integer, Carte> cartes){
		this.cartes = cartes;
	}
	
	public HashMap<Integer, Carte> getCartes(){
		return this.cartes;
	}
	
	public Carte getCarte(int numCarte){
		return getCartes().get(numCarte);
	}
	
	public int getNbMaisons() {
		return nbMaisons;
	}
	
	public int getNbHotels() {
		return nbHotels;
	}
	
	public ArrayList<Carreau> getCarreaux() {
		return carreaux;
	}
	
	public ArrayList<Joueur> getJoueurs() {
		return joueurs;
	}
	
	public Carreau getCarreau(int key){
		return getCarreaux().get(key-1);
	}
	
	public BancDeTest getTest() {
		return test;
	}
	
	private void setNbMaisons(int nbMaisons) {
		this.nbMaisons = nbMaisons;
	}

	private void setTest(BancDeTest test) {
		this.test = test;
	}


	private void setNbHotels(int nbHotels) {
		this.nbHotels = nbHotels;
	}
	
	private void setCarreaux(ArrayList<Carreau> carreaux) {
		this.carreaux = carreaux;
	}
	
	private void setJoueurs(ArrayList<Joueur> joueurs) {
		this.joueurs = joueurs;
	}
	
	// Methods
	/**
	 * Ajoute un joueur au monopoly s'il n'existe pas déjà.
	 * @param j : Le joueur à ajouter
	 */
	public void ajouterJoueur(Joueur j){
		boolean dejaPresent = false;
		int compteur = 0;
		
		while(!dejaPresent && compteur < getJoueurs().size()){
			if(getJoueurs().get(compteur).equals(j)){
				dejaPresent = true;
			}
			compteur++;
		}
		
		if(!dejaPresent){
			addJoueur(j);
		}
	}
	
	/**
	 * Ajoute le joueur au monopoly
	 * @param j : Le joueur à ajouter
	 */
	private void addJoueur(Joueur j){
		getJoueurs().add(j);
	}
	
	/**
	 * Joue un coup
	 * @param j : le joueur qui joue le coup
	 */
	public void jouerUnCoup(Joueur j){
		boolean aFaitUnDouble;
		int nbDoubles = 0;
		do{
			aFaitUnDouble = false;
			if(nbDoubles==3){
				this.getCarreau(31).action(j); // Case aller en prison
			}else if(j.enPrison()){
				boolean choix = false;
				Affichage.AfficherConfirmation(j.getNomJoueur()+" est actuellement en prison");
				if(j.getNbCartePrison()!=0){
					choix = Affichage.PoserQuestionOuiNon("Vous avez "+j.getNbCartePrison()+" cartes sortir prison \n "
																+ "Voulez vous utiliser une carte pour sortir de prison ?");
				}
				if(choix){
					j.utiliserCarteSortirPrison();		
				}
			}
			int[] lancers = j.lancerLesDes();
			if(j.enPrison()){
				if(lancers[0] == lancers[1]){
					Affichage.AfficherConfirmation(j.getNomJoueur()+" a fait un double! Il sort donc de prison");
					j.sortirPrison();
				}else{
					j.setNbToursPrison(j.getNbToursPrison()+1);
					Affichage.AfficherConfirmation(j.getNomJoueur()+" a obtenu "+j.getLancers()[0]+"+"+j.getLancers()[1]+"="+(j.getLancers()[0]+j.getLancers()[1]));
					if(j.getNbToursPrison() == 3){
						j.payer(50);
						j.sortirPrison();
						Affichage.AfficherConfirmation(j.getNomJoueur()+" est resté 3 tours en prison, il sort donc de prison et paye l'amande de 50€");
					}
				}
			}	
			if(!j.enPrison()){
				if(lancers[0] == lancers[1]){
					nbDoubles++;
					if(nbDoubles==3){
						Affichage.AfficherConfirmation(j.getNomJoueur()+" a obtenu "+j.getLancers()[0]+"+"+j.getLancers()[1]+"="+(j.getLancers()[0]+j.getLancers()[1])+" ce qui fait le 3e double. "+j.getNomJoueur()+" va en prison.");
						this.getCarreau(31).action(j); // Case aller en prison
					}
				}
			}
			if(!j.enPrison()){
				int numCarreau = j.getPositionCourante().getNumero();
				numCarreau += lancers[0]+lancers[1];
				if(numCarreau>40){
					j.recevoir(200);
					numCarreau %= 40;
				}
				Carreau carreau = getCarreau(numCarreau);
				j.setPositionCourante(carreau);
				
				Affichage.AfficherConfirmation(j.getNomJoueur()+" a obtenu "+j.getLancers()[0]+"+"+j.getLancers()[1]+"="+(j.getLancers()[0]+j.getLancers()[1])+" et arrive sur "+carreau.getNomCarreau());
				getFenetreUI().repaint();
				carreau.action(j);
				if(j != null){
					getJoueursUI().repaint();
					if(lancers[0] == lancers[1]){
						Affichage.AfficherConfirmation(j.getNomJoueur()+" a fait un double et rejoue !");
						aFaitUnDouble=true;
					}
				}
			}
		}while(aFaitUnDouble);
		
	}
	
	/**
	 * 	Lancer les dés et avancer (en version IHM)
	 * @author Bertrand 
	 * @param j : le joueur qui joue le coup
	 */
	public void jouerUnCoupIHM(final Joueur j){
		getJoueursUI().repaint();
		getFenetreUI().repaint();

		final JButton jouerUnCoup = ((JButton) getJoueursUI().getCases().get(j).getComponent(1));
		final JButton construire = ((JButton) getJoueursUI().getCases().get(j).getComponent(2));
		
		jouerUnCoup.setEnabled(true);
		construire.setEnabled(!j.getProprietesAConstruire().isEmpty());
	}

	/**
	 * @author Bertrand
	 * @param j
	 * @param c
	 */
	public boolean afficherMessage(Joueur j, CarreauPropriete c) {
		this.afficherProprietes(j);
		
		int p = c.getPrixAchat();
		Affichage.Afficher("Prix de "+c.getNomCarreau()+": "+p+"€");
		int cash = j.getCash();
		Affichage.Afficher("Cash : "+cash+" €");
		boolean choix = Affichage.PoserQuestionOuiNon("Cash : "+j.getCash()+"€ \n"+j.getNomJoueur()+", voulez-vous acheter "+c.getNomCarreau()+" pour "+p+"€ ?");

		return choix;
	}
	
	/**
	 * @param j
	 */
	public void construire(Joueur j) {
		HashMap<String, ProprieteAConstruire> props = new HashMap<String, ProprieteAConstruire>();

		for(ProprieteAConstruire p : j.getProprietesAConstruire()){
			props.put(p.getNomCarreau(),p);
		}
		
		String choix = Affichage.FaireChoixDansListe("Où voulez-vous construire ?", props.keySet().toArray() );
		
		ProprieteAConstruire propriete = props.get(choix); // la propriété sélectionnée
		Groupe groupe = propriete.getGroupe();
		boolean ok = true;
		for(ProprieteAConstruire p : groupe.getProprietes()){
			try{ // Pour les propriétés qui n'ont pas été achetées...
				if(!p.getProprietaire().equals(j))
					ok=false;
			}catch(Exception e){ok=false;}
		}
		if(ok){
			int nbMaisons = propriete.getNbMaisons();
			boolean hotel = propriete.isHotel();
			int nb; boolean h;
			boolean ok2 = true;
			for(ProprieteAConstruire p : groupe.getProprietes()){
				nb = p.getNbMaisons();
				h = p.isHotel();
				if(!(nb>=nbMaisons || (h && !hotel)))
					ok2=false;
			}
			if(ok2){
				int cash = j.getCash();
				if(nbMaisons<4 && getNbMaisons()>0 && !hotel){
					int montant = groupe.getPrixAchatMaison();
					if(cash >= montant){
						j.payer(montant);
						this.setNbMaisons(this.getNbMaisons()-1);
						propriete.ajouterMaison();
						getJoueursUI().repaint();
					}else{
						Affichage.AfficherErreur("Vous n'avez pas assez d'argent !");
					}
				}else if(!hotel && getNbHotels()>0){
					int montant = groupe.getPrixAchatHotel();
					if(cash >= montant){
						j.payer(montant);
						this.setNbMaisons(this.getNbMaisons()+1);
						propriete.construireHotel();
						propriete.retirerMaisons(4);
						getJoueursUI().repaint();
					}else{
						Affichage.AfficherErreur("Vous n'avez pas assez d'argent !");
					}
				}else{
					if(hotel){
						Affichage.AfficherErreur("Cette propriété est déjà un hotel. Vous ne pouvez rien construire de plus.");
					}if(getNbMaisons()==0){
						Affichage.AfficherErreur("Il n'y a plus de maisons de disponible.");
					}else if(getNbHotels()==0){
						Affichage.AfficherErreur("Il n'y a plus d'hotels de disponible.");
					}
				}
			}else{
				Affichage.AfficherErreur("Vous devez équilibrer vos maisons sur toute les propriétés du groupe. <br/> Aucune propriété ne doit avoir deux maisons de plus qu'une autre.");
			}
		}else{
			Affichage.AfficherErreur("Vous devez posséder toute les propriétés du groupe <br/>pour pouvoir construire");
		}
	}

	/**
	 * @author Bertrand
	 * @param j
	 */
	public String afficherProprietes(Joueur j) {
		String prop="";
		Groupe g;
		String n;
		int nb;
		boolean h;
		ArrayList<ProprieteAConstruire> Cp = j.getProprietesAConstruire();
		if(!Cp.isEmpty())
			prop = "**** Propriétés à construire :<br/>";
		for(ProprieteAConstruire p : Cp){
			g = p.getGroupe();
			n = p.getNomCarreau();
			nb = p.getNbMaisons();
			h = p.isHotel();
			prop+="+"+n+"("+g.getCouleur().getLabel()+")";
			if(h){
				prop+="[hotel]";
			}else if(nb!=0){
				if(nb==1){
					prop+="["+nb+" maison]";
				}else{
					prop+="["+nb+" maisons]";
				}
			}
			prop+="<br/>";
		}

		ArrayList<Compagnie> Cc = j.getCompagnies();
		if(!Cc.isEmpty())
			prop+="**** Compagnies :<br/>";
		for(Compagnie c : Cc){
			n = c.getNomCarreau();
			prop+="+"+n+"<br/>";
		}
		
		ArrayList<Gare> Cg = j.getGares();
		if(!Cg.isEmpty())
			prop+="**** Gares :<br/>";
		for(Gare gare : Cg){
			n = gare.getNomCarreau();
			prop+="+"+n+"<br/>";
		}
		Affichage.Afficher(prop);
		return prop;
	}

	/**
	 * tirage d'une carte
	 * @param f
	 */
	public Carte tirerCarte(Famille f) {
		if(getTest() != null){
			int carte = getTest().getCarte();
			if(carte <33){
				return getCarte(carte);
			}
		}	
		if(f == Famille.Chance){
			return this.tirerCarteChance();
		}else{
			return this.tirerCarteCommunaute();
		}
		
	}
	
	/**
	 * tirage d'une carte chance
	 * @return Carte
	 */
	private Carte tirerCarteChance() {
		Carte ca = null;
		while(ca == null || ca.getType() != Famille.Chance){
			Random r = new Random();
			int rand = r.nextInt(this.getCartes().size());
			ca = this.getCarte(rand);
		}
		return ca;
	}

	/**
	 * tirage d'une carte communauté
	 * @return Carte
	 */
	private Carte tirerCarteCommunaute() {
		Carte ca = null;
		do{
			int rand = (int)(Math.random()*(this.getCartes().size()+1));
			ca = this.getCartes().get(rand);
		}while(ca == null || ca.getType() != Famille.Communaute);
		return ca;
	}
	
	/**
	 * Lecture et instanciation des cases du plateau.
	 * @author darbonb, lebocp
	 * @param dataFilename
	 */
	private void buildGamePlateau(String dataFilename){
		try{
			ArrayList<String[]> data = readDataFile(dataFilename, ",");
			HashMap<CouleurPropriete, Groupe> groupes = new HashMap<CouleurPropriete,Groupe>();
			Carreau carreau = null;
			Integer num = null;
			String nomC = null;
			Integer prixAchat = null;
					
			for(int i=0; i<data.size(); ++i){
				String caseType = data.get(i)[0];
				num = Integer.parseInt(data.get(i)[1]);
				nomC = data.get(i)[2];
				
				if(caseType.compareTo("P") == 0){
//					Affichage.Afficher("Propriété :\t" + nomC + "\t@ case " + num);
					int[] loyers = new int[6];
					
					for(int j = 5;j<=10;j++){
						loyers[j-5] = Integer.parseInt(data.get(i)[j]);
					}
					
					Integer prixAchatMaison = Integer.parseInt(data.get(i)[11]);
					Integer prixAchatHotel = Integer.parseInt(data.get(i)[12]);
					prixAchat = Integer.parseInt(data.get(i)[4]);
					CouleurPropriete color = CouleurPropriete.valueOf((data.get(i)[3]));
					Groupe groupe  = groupes.get(color);
					
					if(groupe == null){
						groupe = new Groupe(color,prixAchatMaison,prixAchatHotel);
						groupes.put(color,groupe);
					}
					
					carreau = new ProprieteAConstruire(num,nomC,this,prixAchat,loyers,groupe);
					groupe.ajouterProprieteAConstruire((ProprieteAConstruire) carreau);
				}
				else if(caseType.compareTo("G") == 0){
//					Affichage.Afficher("Gare :\t" + nomC + "\t@ case " + num);
					prixAchat = Integer.parseInt(data.get(i)[3]);
					carreau = new Gare(num, nomC, this, prixAchat);
				}
				else if(caseType.compareTo("C") == 0){
//					Affichage.Afficher("Compagnie :\t" + nomC + "\t@ case " + num);
					prixAchat = Integer.parseInt(data.get(i)[3]);
					carreau = new Compagnie(num, nomC, this, prixAchat);
				}
				else if(caseType.compareTo("CT") == 0){
//					Affichage.Afficher("Case Tirage :\t" + nomC + "\t@ case " + num);
					carreau = new CarreauTirage(num, nomC, this);
				}
				else if(caseType.compareTo("CA") == 0){
//					Affichage.Afficher("Case Argent :\t" + nomC + "\t@ case " + num);
					Integer montant = Integer.parseInt(data.get(i)[3]);
					carreau = new CarreauArgent(num, nomC, this, montant);
				}
				else if(caseType.compareTo("CM") == 0){
//					Affichage.Afficher("Case Mouvement :\t" + nomC + "\t@ case " + num);
					carreau = new CarreauMouvement(num, nomC, this);
				}
				else{
					System.err.println("[buildGamePlateau()] : Invalid Data type");
				}
				
				if(carreau != null){
					getCarreaux().add(carreau);
				}
				carreau = null;
			}
		} 
		catch(FileNotFoundException e){
			System.err.println("[buildGamePlateau()] : File is not found!");
		}
		catch(IOException e){
			System.err.println("[buildGamePlateau()] : Error while reading file!");
		}
	}
	
	/**
	 * Lecture et instanciation des cartes du plateau.
	 * @author lebocp
	 * @since 05/01/2015
	 * @param dataFilename
	 */
	private void buildGameCartes(String dataFilename){
		try{
			ArrayList<String[]> data = readDataFile(dataFilename, ";");
			
			for(int i=0; i<data.size(); ++i){
				String caseType = data.get(i)[0];
				String carteType = data.get(i)[1];
				String texte = data.get(i)[2];
				
				Carte carte = null;
				int montant = 0, montant2 = 0;
				boolean franchirDepart = false;
				Famille famille = null;
				
//				Affichage.Afficher("Carte :\t" + caseType + "\t@ carte " + texte);
				
				if(caseType.compareTo("CH") == 0)
				{
					famille = Famille.Chance;
				}
				else if(caseType.compareTo("CO") == 0)
				{
					famille = Famille.Communaute;
				}
				else
				{
					System.err.println("[buildGameCartes()] : Invalid Data type");
					continue;
				}
				
				switch(carteType)
				{
					case "CA" :
						montant = Integer.parseInt(data.get(i)[3]);
						carte = new CarteArgent(this, famille, texte, montant);
						break;
					case "CR" :
						montant = Integer.parseInt(data.get(i)[3]);
						montant2 = Integer.parseInt(data.get(i)[4]);
						carte = new CarteReparations(this, famille, texte, montant, montant2);
						break;
					case "CMR" :
						montant = Integer.parseInt(data.get(i)[3]);
						carte = new CarteMouvementRelatif(this, famille, texte, montant);
						break;
					case "CMA" :
						montant = Integer.parseInt(data.get(i)[3]);
						franchirDepart = Boolean.parseBoolean(data.get(i)[4]);
						carte = new CarteMouvementAbsolu(this, famille, texte, getCarreau(montant), franchirDepart);
						break;
					case "CP" :
						carte = new CarteSortirPrison(this, famille, texte);
						break;
					case "CB" :
						montant = Integer.parseInt(data.get(i)[3]);
						carte = new CarteAnniversaire(this, famille, texte, montant);
						break;
					default:
						break;
				}
				
				if(carte != null)
				{
					carte.setNumero(i+1);
					getCartes().put(i+1,carte);
				}
			}
		} 
		catch(FileNotFoundException e){
			System.err.println("[buildGameCartes()] : File is not found!");
		}
		catch(IOException e){
			System.err.println("[buildGameCartes()] : Error while reading file!");
		}
	}
	
	/**
	 * 
	 * @param filename
	 * @param token
	 * @return Collection des données du fichier.
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	private ArrayList<String[]> readDataFile(String filename, String token) throws FileNotFoundException, IOException{
		ArrayList<String[]> data = new ArrayList<String[]>();
		
		BufferedReader reader  = new BufferedReader(new FileReader(filename));
		String line = null;
		while((line = reader.readLine()) != null){
			data.add(line.split(token));
		}
		reader.close();
		
		return data;
	}

	public void retirerCarteSortirPrison(Carte ca) {
		this.getCartes().remove(ca.getNumero());
	}
	
	public void ajouterCarteSortirPrison(Carte ca){
		this.getCartes().put(ca.getNumero(),ca);
	}
	
	public void initTest(){
		setTest(new BancDeTest(System.getProperty("user.dir")+"/src/data/testDes.data",System.getProperty("user.dir")+"/src/data/testCartes.data"));
	}
	
	public void initScenario(){
		setTest(new BancDeTest(System.getProperty("user.dir")+"/src/data/scenarioDes.data",System.getProperty("user.dir")+"/src/data/scenarioCartes.data"));
	}
	
	/**
	 * Crée une nouvelle partie et lance le jeu
	 * @author Bertrand
	 */
	public void nouvellePartie(){
		
		//////////////////////////////////
		// Initialisation des variables //
		//////////////////////////////////
		
		boolean saisieOK = false;
		int nombreJoueur = 0;
		String nomJoueur = null;
		
		/////////////////////////////
		// Initialiser une partie. //
		/////////////////////////////
		
		

		// Création des joueurs
		// Nombre de joueur désiré
		
		do{
			try{
				nombreJoueur = Affichage.DemanderValeur("Quel nombre de joueurs désirez vous ? ");

				if(nombreJoueur > 1 && nombreJoueur <= 6){
					saisieOK = true;
				}else{
					Affichage.AfficherErreur("Vous devez saisir un nombre entier entre 2 et 6 inclus.");
				}
			}catch(Exception e){
				Affichage.AfficherErreur("Vous devez saisir un nombre entier entre 2 et 6 inclus.");
			}
		}while(!saisieOK);
		
		// Instanciation des joueurs et affectation dans le monopoly
		for(int i=1;i<=nombreJoueur;i++){
			boolean ok = false;
			do{
				try{
					nomJoueur = Affichage.DemanderNom("Joueur ["+i+"] : Sous quel nom désirez vous jouer ? ");
					ok = true;
					for(Joueur j : this.getJoueurs()){
						if(j.getNomJoueur().equals(nomJoueur)){
							ok = false;
						    Affichage.AfficherErreur("Les noms de joueur doivent être différent");
						}
					}
					if(ok)
						this.ajouterJoueur(new Joueur(nomJoueur, this, this.getCarreau(1)));
				}catch(Exception e){
					Affichage.AfficherErreur("Nom de joueur incorrect.");
				}
			}while(!ok);
		}
		setJoueursUI(new JoueursUI(this));
		getJoueursUI().afficher();
		// Tirage des dés (choix du premier joueur)
		Affichage.Afficher(":: Sélection du premier joueur ::");
		HashMap<Joueur, Integer> scoresLance = new HashMap<Joueur,Integer>();
		ArrayList<Joueur> joueurs = this.getJoueurs();
		int[] lancer = new int[2];
		int t;
		
		for(Joueur j : joueurs){
			lancer = j.lancerLesDes();
			t = lancer[0]+lancer[1];
			scoresLance.put(j, t);
			Affichage.Afficher(j.getNomJoueur()+" a obtenu "+t);
		}
		
		// TODO : prendre en compte si 2 joueurs ont fait le même lancer de dés
		Joueur prochainJ = joueurs.get(0);
		for(Joueur j : joueurs){
			if(scoresLance.get(j)>scoresLance.get(prochainJ)){
				prochainJ = j;
			}
		}
		
//		int k = joueurs.indexOf(prochainJ);
		Affichage.AfficherConfirmation(prochainJ.getNomJoueur()+" commence !");
		setProchainJ(prochainJ);
		initActionListeners();
		jouerUnCoupIHM(getProchainJ());
//		do{
//			getJoueursUI().repaint();
//			getFenetreUI().repaint();
//			this.jouerUnCoupIHM(prochainJ);
//			k++;
//			k %= joueurs.size();
//			prochainJ = joueurs.get(k);
//			setProchainJ(prochainJ);
//		}while(joueurs.size()!=1);
	}

	/**
	 * Initialise les ActionListeners sur les boutons de tout les joueurs
	 * @author Bertrand
	 */
	private void initActionListeners(){
		for(final Joueur j : getJoueurs()){
			final JButton jouerUnCoup = ((JButton) getJoueursUI().getCases().get(j).getComponent(1));
			final JButton construire = ((JButton) getJoueursUI().getCases().get(j).getComponent(2));	
			final JButton finDuTour = ((JButton) getJoueursUI().getCases().get(j).getComponent(3));			

			jouerUnCoup.setEnabled(false);
			construire.setEnabled(false);
			finDuTour.setEnabled(false);
			
			finDuTour.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {

					int k = getJoueurs().indexOf(j);
					k++;
					k %= joueurs.size();
					prochainJ = getJoueurs().get(k);
					setProchainJ(prochainJ);
					jouerUnCoup.setEnabled(false);//Bouton Jouer un coup
					construire.setEnabled(false);//Bouton fin du tour
					finDuTour.setEnabled(false);//Bouton fin du tour
					jouerUnCoupIHM(getProchainJ());
					
				}
				
			});
			
			construire.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					construire(j);
					finDuTour.setEnabled(true);
				}
			});
			
			jouerUnCoup.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					jouerUnCoup(j);
					construire.setEnabled(!j.getProprietesAConstruire().isEmpty());
					jouerUnCoup.setEnabled(false);
					finDuTour.setEnabled(true);
					if(j.getCash()<0){
						getJoueurs().remove(j);
					}
					if(getJoueurs().size()==1){
						finPartie();
					}
				}
			});

		}
        getJoueursUI().addKeyListener(new KeyListener() {
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode()==112){
					Affichage.AfficherErreur("Cheat Mode activé !!");
					int cheat;
					for(Joueur j : getJoueurs()){
						cheat=Affichage.DemanderValeur("Combien voulez-vous donner à "+j.getNomJoueur()+" ?");
						j.recevoir(cheat);
						getJoueursUI().repaint();
					}
				}
			}
			@Override
			public void keyTyped(KeyEvent e) {}
			@Override
			public void keyReleased(KeyEvent e) {}
		});	
        getJoueursUI().addKeyListener(new KeyListener() {
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode()==113){
					Affichage.AfficherErreur("Cheat Mode activé !!");
					int cheat;
					for(Joueur j : getJoueurs()){
						cheat=Affichage.DemanderValeur("Où voulez-vous téléporter "+j.getNomJoueur()+" ? (Numéro de case : 0 pour laisser sur place)");
						if(cheat!=0){
							j.setPositionCourante(getCarreau(cheat));
							getCarreau(cheat).action(j);
							getFenetreUI().repaint();
							getJoueursUI().repaint();
						}
					}
				}
			}
			@Override
			public void keyTyped(KeyEvent e) {}
			@Override
			public void keyReleased(KeyEvent e) {}
		});	
	}
	
	/**
	 * @author Bertrand
	 */
	private void finPartie() {
		Joueur j =getJoueurs().get(0);
		Affichage.AfficherConfirmation(j.getNomJoueur()+" est le dernier joueur sur le plateau ! \n "+j.getNomJoueur()+" a gagné !");
		if(Affichage.PoserQuestionOuiNon("Voulez-vous quitter le jeu ?(ou jouer tout seul ?)")){
			System.exit(0);
		}
	}

	public JoueursUI getJoueursUI() {
		return joueursUI;
	}


	public void setJoueursUI(JoueursUI joueursUI) {
		this.joueursUI = joueursUI;
	}


	public void setProchainJ(Joueur prochainJ) {
		this.prochainJ = prochainJ;
	}


	public Joueur getProchainJ(){
		return prochainJ;
	}

	/**
	 * Ouvre la fenêtre plateau de jeu
	 * @author Bertrand
	 */
	private void initUI() {
		setFenetreUI(new FenetreUI(this));
		getFenetreUI().afficher();
	}

	public FenetreUI getFenetreUI() {
		return fenetreUI;
	}

	private void setFenetreUI(FenetreUI fenetreUI) {
		this.fenetreUI = fenetreUI;
	}

	/**
	 * Pour savoir si une UI doit être affichée ou non
	 * @author Bertrand
	 * @return
	 */
	public boolean isUI() {
		return UI;
	}

	private void setUI(boolean uI) {
		UI = uI;
	}
}
