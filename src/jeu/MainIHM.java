package jeu;
/**
 * 
 * @author Bertrand
 *
 */
public class MainIHM {
	
	public static void main(String[] args) {
		// Création du monopoly
		Println("Création du monopoly...");
		new Monopoly(System.getProperty("user.dir")+"/src/data/case.data",true);
	}
	
	/**
	 * Raccourci pour System.out.println(String text);
	 * @param text
	 */
	private static void Println(String text){
		System.out.println(text);
	}
}