package jeu;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

import utilitaire.Affichage;

public class MainTest {
	
	private static final Scanner scanner = new Scanner(System.in);
	
	public static void main(String[] args) {
		
		//////////////////////////////////
		// Initialisation des variables //
		//////////////////////////////////
		
		boolean saisieOK = false;
		int nombreJoueur = 0;
		String nomJoueur = null;
		
		/////////////////////////////
		// Initialiser une partie. //
		/////////////////////////////
		
		// Création du monopoly
		Println("Création du monopoly...");
		Monopoly monopoly = new Monopoly(System.getProperty("user.dir")+"/src/data/case.data");
		
		
		//Banc de test
		monopoly.initTest();
		
		// Création des joueurs
		// Nombre de joueur désiré
		
		do{
			Print("Quel nombre de joueurs désirez vous ? ");
			try{
				nombreJoueur = scanner.nextInt();
				
				if(nombreJoueur >= 2 && nombreJoueur <= 6){
					saisieOK = true;
				}else{
					Println("Vous devez saisir un nombre entier entre 2 et 6 inclus.");
				}
			}catch(Exception e){
				Println("Vous devez saisir un nombre entier entre 2 et 6 inclus.");
				scanner.nextLine();
			}
		}while(!saisieOK);
		
		// Instanciation des joueurs et affectation dans le monopoly
		for(int i=1;i<=nombreJoueur;i++){
			Print("Joueur ["+i+"] : Sous quel nom désirez vous jouer ? ");
			boolean ok = false;
			do{
				try{
					nomJoueur = scanner.next(); // debug (Scanner pose problème pour pas changer....)
					scanner.nextLine();
					ok = true;
					for(Joueur j : monopoly.getJoueurs()){
						if(j.getNomJoueur().equals(nomJoueur)){
							ok = false;
						    Affichage.AfficherErreur("Les noms de joueur doivent être différent");
						}
					}
					if(ok)
						monopoly.ajouterJoueur(new Joueur(nomJoueur, monopoly, monopoly.getCarreau(1)));
				}catch(Exception e){
					Println("Nom de joueur incorrect.");
					scanner.next();
				}
			}while(!ok);
		}
		
		// Tirage des dés (choix du premier joueur)
		Println(":: Sélection du premier joueur ::");
		HashMap<Joueur, Integer> scoresLance = new HashMap<Joueur,Integer>();
		ArrayList<Joueur> joueurs = monopoly.getJoueurs();
		int[] lancer = new int[2];
		int t;
		
		for(Joueur j : joueurs){
			lancer = j.lancerLesDes();
			t = lancer[0]+lancer[1];
			scoresLance.put(j, t);
			Println(j.getNomJoueur()+" a obtenu "+t);
		}
		
		// TODO : prendre en compte si 2 joueurs ont fait le même lancer de dés
		Joueur prochainJ = joueurs.get(0);
		for(Joueur j : joueurs){
			if(scoresLance.get(j)>scoresLance.get(prochainJ)){
				prochainJ = j;
			}
		}
		
		int k = joueurs.indexOf(prochainJ);
		Println(prochainJ.getNomJoueur()+" commence !");
		do{
			monopoly.jouerUnCoup(prochainJ);
			k++;
			k %= joueurs.size();
			prochainJ = joueurs.get(k);
		}while(joueurs.size()!=1);
	}
	
	/**
	 * Raccourci pour System.out.println(String text);
	 * @param text
	 */
	private static void Println(String text){
		System.out.println(text);
	}
	
	/**
	 * Raccourci pour System.out.print(String text);
	 * @param text
	 */
	private static void Print(String text){
		System.out.print(text);
	}
}