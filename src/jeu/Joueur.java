package jeu;

import java.util.*;

import utilitaire.Affichage;
import data.Carreau;
import data.Carte;
import data.CarteSortirPrison;
import data.Compagnie;
import data.Gare;
import data.ProprieteAConstruire;

public class Joueur {
	
	// Attributs
	private Monopoly monopoly;
	private Carreau positionCourante;
	private ArrayList<ProprieteAConstruire> proprietesAConstruire;
	private ArrayList<Compagnie> compagnies;
	private ArrayList<Gare> gares;
	private String nomJoueur;
	private int cash;
	private int[] lancers;
	private boolean prison;
	private int nbToursPrison;
	private CarteSortirPrison[] cartesSortirPrison;
	
	// Constructeurs
	public Joueur(String nomJoueur, Monopoly monopoly, Carreau position){
		setNomJoueur(nomJoueur);
		setCash(1500);
		setMonopoly(monopoly);
		setPositionCourante(position);
		
		setCompagnies(new ArrayList<Compagnie>());
		setGares(new ArrayList<Gare>());
		setCompagnies(new ArrayList<Compagnie>());
		setProprietesAConstruire(new ArrayList<ProprieteAConstruire>());
		setPrison(false);
		setLancers(new int[2]);
		setCartesSortirPrison(new CarteSortirPrison[2]);
		setNbToursPrison(0);
	}
	
	// Getters & Setters
	private void setMonopoly(Monopoly monopoly) {
		this.monopoly = monopoly;
	}
	
	public Monopoly getMonopoly(){
		return this.monopoly;
	}

	private void setProprietesAConstruire(ArrayList<ProprieteAConstruire> proprietesAConstruire) {
		this.proprietesAConstruire = proprietesAConstruire;
	}

	private void setCompagnies(ArrayList<Compagnie> compagnies) {
		this.compagnies = compagnies;
	}

	private void setGares(ArrayList<Gare> gares) {
		this.gares = gares;
	}

	private void setNomJoueur(String nomJoueur) {
		this.nomJoueur = nomJoueur;
	}

	private void setCash(int cash) {
		this.cash = cash;
	}

	public void setLancers(int[] lancers) {
		this.lancers = lancers;
	}
	
	private void setCartesSortirPrison(CarteSortirPrison[] cartesSortirPrison) {
		this.cartesSortirPrison = cartesSortirPrison;
	}
	
	public void setNbToursPrison(int tours){
		this.nbToursPrison = tours;
	}

	public int getNbToursPrison(){
		return nbToursPrison;
	}
	
	public int getCash() {
		return this.cash;
	}
	
	public ArrayList<ProprieteAConstruire> getProprietesAConstruire() {
		Collections.sort(proprietesAConstruire);
		return proprietesAConstruire;
	}

	public ArrayList<Compagnie> getCompagnies() {
		return compagnies;
	}

	public ArrayList<Gare> getGares() {
		return gares;
	}
	
	public Carreau getPositionCourante() {
		return this.positionCourante;
	}
	
	public void setPositionCourante(Carreau carreau) {
		this.positionCourante = carreau;
	}
	
	public int[] getLancers() {
		return this.lancers;
	}

	public String getNomJoueur() {
 		return this.nomJoueur;
	}
	
	public CarteSortirPrison[] getCartesSortirPrison() {
		return cartesSortirPrison;
	}
	
	public void recevoir(int montant) {
		this.setCash(this.getCash()+montant);
	}
	
	/**
	 * Vérifie que la propriete n'appartient pas au joueur, et si ce n'est pas le cas, l'ajoute à la liste de ses propriétés à construire
	 * @param e : la propriété à ajouter.
	 */
	public void ajouterProprieteAConstruire(ProprieteAConstruire e){
		boolean dejaPresent = false;
		int compteur = 0;
		
		while(!dejaPresent && compteur < getProprietesAConstruire().size()){
			if(getProprietesAConstruire().get(compteur).equals(e))
				dejaPresent = true;
			compteur++;
		}
		
		if(!dejaPresent)
			addProprieteAConstruire(e);
	}
	
	/**
	 * Vérifie que la compagnie n'appartient pas déjà au joueur, et si ce n'est pas le cas l'ajoute à la liste de ses compagnies.
	 * @param e : la compagnie à ajouter
	 */
	public void ajouterCompagnie(Compagnie e){
		boolean dejaPresent = false;
		int compteur = 0;
		
		while(!dejaPresent && compteur < getCompagnies().size()){
			if(getCompagnies().get(compteur).equals(e))
				dejaPresent = true;
			compteur++;
		}
		
		if(!dejaPresent)
			addCompagnie(e);
	}
	
	/**
	 * Vérifie que la gare n'appartient pas déjà au joueur, et si ce n'est pas le cas l'ajoute à la liste de ses gares.
	 * @param e : la gare à ajouter
	 */
	public void ajouterGare(Gare e){
		boolean dejaPresent = false;
		int compteur = 0;
		
		while(!dejaPresent && compteur < getGares().size()){
			if(getGares().get(compteur).equals(e))
				dejaPresent = true;
			compteur++;
		}
		
		if(!dejaPresent)
			addGare(e);
	}
	
	private void addProprieteAConstruire(ProprieteAConstruire e){
		getProprietesAConstruire().add(e);
	}
	
	private void addCompagnie(Compagnie e){
		getCompagnies().add(e);
	}
	
	private void addGare(Gare e){
		getGares().add(e);
	}
	
	/**
	 * Retire une propriété de la liste du joueur si elle y est présente.
	 * @param propriete : la propriété à retirer
	 */
	public void supprimerProprieteAConstruire(ProprieteAConstruire propriete){
		int compteur = 0;
		boolean trouve = false;
		
		while(!trouve && compteur < getProprietesAConstruire().size()){
			if(getProprietesAConstruire().get(compteur).equals(propriete))
				trouve = true;
			compteur++;
		}
		
		if(trouve)
			delProprieteAConstruire(propriete);
	}
	
	/**
	 * Retire une gare de la liste du joueur si elle y est présente.
	 * @param gare : la gare à retirer
	 */
	public void supprimerGare(Gare gare){
		int compteur = 0;
		boolean trouve = false;
		
		while(!trouve && compteur < getGares().size()){
			if(getGares().get(compteur).equals(gare))
				trouve = true;
			compteur++;
		}
		
		if(trouve)
			delGare(gare);
	}
	
	/**
	 * Retire une gare de la lsite du joueur si elle y est présente.
	 * @param compagnie : la gare à retirer
	 */
	public void supprimerCompagnie(Compagnie compagnie){
		int compteur = 0;
		boolean trouve = false;
		
		while(!trouve && compteur < getCompagnies().size()){
			if(getCompagnies().get(compteur).equals(compagnie))
				trouve = true;
			compteur++;
		}
		
		if(trouve)
			delCompagnie(compagnie);
	}
	
	private void delProprieteAConstruire(ProprieteAConstruire propriete){
		getProprietesAConstruire().remove(propriete);
	}
	
	private void delGare(Gare gare) {
		getGares().remove(gare);
	}
	
	private void delCompagnie(Compagnie compagnie) {
		getCompagnies().remove(compagnie);
	}
	
	private void enFaillite(){
		Affichage.AfficherErreur("Le joueur "+this.getNomJoueur()+" n'a plus assez d'argent, il a donc perdu");
		for(ProprieteAConstruire p:this.getProprietesAConstruire()){
			p.viderPropriete();
		}
		getProprietesAConstruire().clear();
		for(Compagnie c:this.getCompagnies()){
			c.viderPropriete();
		}
		getCompagnies().clear();
		for(Gare g:this.getGares()){
			g.viderPropriete();
		}
		getGares().clear();
	}
	
	/**
	* Déduit le montant du cash du joueur.
	* @param montant
	*/
	public int payer(int montant){
		if(getCash()>= montant){
			setCash(getCash()-montant);
			return montant;
		}else{
			enFaillite();
			setCash(getCash()-montant);
			return getCash();
		}
	}

	/**
	 * Tire deux nombres aléatoire pour lancer les dés.
	 * @return le tableau contenant les deux lancers
	 */
	public int[] lancerLesDes(){
		if(getMonopoly().getTest() != null){
			lancers = getMonopoly().getTest().getLancer();
			if(lancers[0] != 0 && lancers[1] != 0){
				return lancers;
			}
		}
			lancers[0] = new Random().nextInt(6)+1;
			lancers[1] = new Random().nextInt(6)+1;
		return lancers;
	}
	
	public int getNbCompagnies() {
		return getCompagnies().size();
	}

	public int getNbGares() {
		return getGares().size();
	}

	public int getNbMaisons() {
		int nbMaisons = 0;
		for(ProprieteAConstruire p: getProprietesAConstruire()){
			nbMaisons += p.getNbMaisons();
		}
		return nbMaisons;
	}

	public int getNbHotels() {
		int nbHotels = 0;
		for(ProprieteAConstruire p: getProprietesAConstruire()){
			if(p.isHotel()){
				nbHotels++;
			}
		}
		return nbHotels;
	}

	public boolean enPrison() {
		return prison;
	}

	public void setPrison(boolean prison) {
		this.prison = prison;
	}

	public void ajouterCarteSortirPrison(Carte ca) {
		CarteSortirPrison[] listeCartes = this.getCartesSortirPrison();
		if(listeCartes[0] == null){
			listeCartes[0] = (CarteSortirPrison) ca;
		}else if(listeCartes[1] == null){
			listeCartes[1] = (CarteSortirPrison) ca;
		}
		this.setCartesSortirPrison(listeCartes);
		this.getMonopoly().retirerCarteSortirPrison(ca);
	}
	
	public int getNbCartePrison(){
		if(getCartesSortirPrison()[0]!= null && getCartesSortirPrison()[1]!=null){
			return 2;
		}else if(getCartesSortirPrison()[0]!= null || getCartesSortirPrison()[1]!=null){
			return 1;
		}else{
			return 0;
		}
	}
	
	public boolean utiliserCarteSortirPrison(){
		if(this.enPrison() == true){
			if(getNbCartePrison()!=0){
				if(getCartesSortirPrison()[1]!=null){
					this.sortirPrison();
					this.getMonopoly().ajouterCarteSortirPrison(getCartesSortirPrison()[1]);
					getCartesSortirPrison()[1] = null;
					return true;
				}else{
					this.sortirPrison();
					this.getMonopoly().ajouterCarteSortirPrison(getCartesSortirPrison()[0]);
					getCartesSortirPrison()[0] = null;
					return true;
				}
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
	
	public void sortirPrison(){
		this.setPrison(false);
		this.setNbToursPrison(0);
	}
}